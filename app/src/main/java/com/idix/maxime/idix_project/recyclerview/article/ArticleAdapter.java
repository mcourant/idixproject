package com.idix.maxime.idix_project.recyclerview.article;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.idix.maxime.idix_project.R;
import com.idix.maxime.idix_project.item.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder> {

    private List<Article> allArticle;
    private Context context;

    public ArticleAdapter(List<Article> allArticle, Context context){
        this.allArticle = allArticle;
        this.context = context;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewName = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_article, parent, false);
        return new ArticleViewHolder(viewName);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        holder.getTitleArticle().setText(allArticle.get(position).getT());
        holder.getSectionArticle().setText(allArticle.get(position).getR().getN());

        ImageView imageView = (ImageView) holder.getImageViewArticle();
        Picasso.with(context)
                .load(allArticle.get(position).getI().getU())
                .resize(50, 40) // here you resize your image to whatever width and height you like
                .into(imageView);

    }

    @Override
    public int getItemCount() {
        return allArticle.size();
    }

}

