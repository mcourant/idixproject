package com.idix.maxime.idix_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.idix.maxime.idix_project.R;
import com.idix.maxime.idix_project.item.Article;
import com.idix.maxime.idix_project.item.Author;
import com.idix.maxime.idix_project.item.Image;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class ArticleActivity extends AppCompatActivity {

    private Article article = new Article();
    private Image image;
    private int id;
    private Author author;
    private TextView textViewTitle;
    private ImageView imageView;
    private TextView textViewChapo;
    private TextView textViewText;
    private TextView textViewAuthor;
    private TextView textViewDate;
    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        textViewTitle = (TextView) findViewById(R.id.articleViewTitle);
        imageView = (ImageView) findViewById(R.id.articleViewImage);
        textViewChapo = (TextView) findViewById(R.id.articleViewChapo);
        textViewText = (TextView) findViewById(R.id.articleViewText);
        textViewAuthor = (TextView) findViewById(R.id.articleViewAuthor);
        textViewDate = (TextView) findViewById(R.id.articleViewDate);


        Intent intent = getIntent();
        article = intent.getParcelableExtra("article");
        image = intent.getParcelableExtra("image");
        author = intent.getParcelableExtra("author");
        id = intent.getIntExtra("id",0);

        // client HTTP :
        AsyncHttpClient client = new AsyncHttpClient();
        // paramètres :
        final RequestParams requestParams = new RequestParams();
        client.post("http://www.femininbio.com/json/article/"+id, requestParams, new AsyncHttpResponseHandler()   {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)      {
                String retour = new String(response);
                try {
                    JSONObject article = new JSONObject(retour);
                    text = (String) article.get("co");
                    textViewText.setText(Html.fromHtml(text).toString());
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers,byte[] errorResponse, Throwable e)      {
                Log.e("i", e.toString());
            }
        });

        textViewTitle.setText(article.getT());
        Picasso.with(this)
                .load(image.getU())
                .resize(300,200)
                .into(imageView);
        textViewChapo.setText(article.getCh());
        textViewAuthor.setText(author.getN());


        Date d = new Date( Integer.parseInt(article.getD())*1000L);
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String text = df.format(d);
        textViewDate.setText(text);

    }
}
