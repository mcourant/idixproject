package com.idix.maxime.idix_project.item;

import java.util.ArrayList;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class ArticleDTO {

    private int id;
    private String d;
    private String t;
    private Image i;
    private Section r;
    private String ch;
    private String co;
    private Author a;
    private ArrayList<ArticleRelated> al;

    public ArticleDTO() {
    }

    public ArticleDTO(int id, String d, String t, Image i, Section r, String ch, String co, Author a, ArrayList<ArticleRelated> al) {
        this.id = id;
        this.d = d;
        this.t = t;
        this.i = i;
        this.r = r;
        this.ch = ch;
        this.co = co;
        this.a = a;
        this.al = al;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Image getI() {
        return i;
    }

    public void setI(Image i) {
        this.i = i;
    }

    public Section getR() {
        return r;
    }

    public void setR(Section r) {
        this.r = r;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public Author getA() {
        return a;
    }

    public void setA(Author a) {
        this.a = a;
    }

    public ArrayList<ArticleRelated> getAl() {
        return al;
    }

    public void setAl(ArrayList<ArticleRelated> al) {
        this.al = al;
    }
}
