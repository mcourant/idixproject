package com.idix.maxime.idix_project.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class Image implements Parcelable {

    private String u;
    private String l;

    public Image(){

    }

    public Image(String u, String l){
        this.u = u;
        this.l = l;
    }

    protected Image(Parcel in) {
        u = in.readString();
        l = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public String getU() {
        return u;
    }

    public void setU(String u) {
        this.u = u;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(u);
        parcel.writeString(l);
    }
}
