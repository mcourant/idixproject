package com.idix.maxime.idix_project.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class Author implements Parcelable{

    private int id;
    private String n;
    private String u;

    public Author(){

    }

    public Author(int id, String n, String u){
        this.id = id;
        this.n = n;
        this.u = u;
    }


    protected Author(Parcel in) {
        id = in.readInt();
        n = in.readString();
        u = in.readString();
    }

    public static final Creator<Author> CREATOR = new Creator<Author>() {
        @Override
        public Author createFromParcel(Parcel in) {
            return new Author(in);
        }

        @Override
        public Author[] newArray(int size) {
            return new Author[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getU() {
        return u;
    }

    public void setU(String u) {
        this.u = u;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(n);
        parcel.writeString(u);
    }
}
