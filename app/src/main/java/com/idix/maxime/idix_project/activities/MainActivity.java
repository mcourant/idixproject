package com.idix.maxime.idix_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;


import com.google.gson.Gson;
import com.idix.maxime.idix_project.R;
import com.idix.maxime.idix_project.converter.ArticleConverter;
import com.idix.maxime.idix_project.item.Article;
import com.idix.maxime.idix_project.item.ArticleDTO;
import com.idix.maxime.idix_project.recyclerview.article.ArticleAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements RecyclerView.OnItemTouchListener {

    private RecyclerView recyclerViewForArticle;
    private RecyclerView.Adapter adapterRecyclerView;
    private RecyclerView.LayoutManager layoutManagerRecyclerView;

    private ArticleDTO article_dto;
    private List<Article> listArticle = new ArrayList<>();

    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        // client HTTP :
        AsyncHttpClient client = new AsyncHttpClient();
        // paramètres :
        final RequestParams requestParams = new RequestParams();
        client.post("http://www.femininbio.com/json/liste-articles", requestParams, new AsyncHttpResponseHandler()   {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)      {
                String retour = new String(response);
                try {
                    JSONArray jsonArray = new JSONArray(retour);
                    for (int i=0; i< jsonArray.length(); i++){
                        JSONObject article = jsonArray.getJSONObject(i);
                        Gson gson = new Gson();
                        article_dto = gson.fromJson(String.valueOf(article), ArticleDTO.class);
                        ArticleConverter converter = new ArticleConverter();
                        listArticle.add(converter.formDTO(article_dto, MainActivity.this));


                    }
                    adapterRecyclerView = new ArticleAdapter(listArticle, getApplicationContext());
                    recyclerViewForArticle.setAdapter(adapterRecyclerView);
                }catch (Exception e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers,byte[] errorResponse, Throwable e)      {
                Log.e("i", e.toString());
            }
        });


        recyclerViewForArticle = (RecyclerView) findViewById(R.id.articleRecyclerView);

        recyclerViewForArticle.setHasFixedSize(true);

        layoutManagerRecyclerView = new LinearLayoutManager(this);
        recyclerViewForArticle.setLayoutManager(layoutManagerRecyclerView);

        recyclerViewForArticle.addOnItemTouchListener(this);

        recyclerViewForArticle.addItemDecoration(new
                DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));



        gestureDetector = new GestureDetector(this,new GestureDetector.SimpleOnGestureListener()      {
            @Override
            public boolean onSingleTapUp(MotionEvent event){
                return true;
            }
        });

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        if (gestureDetector.onTouchEvent(e))      {
            View child = rv.findChildViewUnder(e.getX(),e.getY());
            if (child != null){
                int position = rv.getChildAdapterPosition(child);

                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                intent.putExtra("id",listArticle.get(position).getId());
                intent.putExtra("article", listArticle.get(position));
                intent.putExtra("image", listArticle.get(position).getI());
                intent.putExtra("author", listArticle.get(position).getA());
                startActivity(intent);

                return true;
            }
        }return false;
    }


    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
