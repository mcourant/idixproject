package com.idix.maxime.idix_project.item;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class ArticleRelated {

    private int id;
    private String t;

    public ArticleRelated(){

    }

    public ArticleRelated(int id, String t) {
        this.id = id;
        this.t = t;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getT() {
        return t;
    }

    public void setT(String name) {
        this.t = t;
    }
}
