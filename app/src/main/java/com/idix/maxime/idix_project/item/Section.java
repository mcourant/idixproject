package com.idix.maxime.idix_project.item;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class Section {

    private int id;
    private String n;

    public Section(){

    }

    public Section(int id, String n){
        this.id = id;
        this.n = n;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }
}
