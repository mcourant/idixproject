package com.idix.maxime.idix_project.converter;

import android.content.Context;

import com.idix.maxime.idix_project.item.Article;
import com.idix.maxime.idix_project.item.ArticleDTO;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class ArticleConverter implements Converter<ArticleDTO, Article>{

    private Article article = new Article();

    @Override
    public Article formDTO(ArticleDTO aDto, Context context) {
        article.setA(aDto.getA());
        article.setAl(aDto.getAl());
        article.setCh(aDto.getCh());
        article.setCo(aDto.getCo());
        article.setD(aDto.getD());
        article.setI(aDto.getI());
        article.setId(aDto.getId());
        article.setR(aDto.getR());
        article.setT(aDto.getT());

        return article;
    }

    @Override
    public ArticleDTO toDTO(Article model) {
        return null;
    }
}
