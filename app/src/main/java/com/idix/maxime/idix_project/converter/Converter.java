package com.idix.maxime.idix_project.converter;

import android.content.Context;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public interface Converter<T, U> {
    U formDTO(T aDto, Context context);
    T toDTO(U model);
}
