package com.idix.maxime.idix_project.recyclerview.article;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.idix.maxime.idix_project.R;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class ArticleViewHolder  extends RecyclerView.ViewHolder {

    private ImageView imageViewArticle;
    private TextView titleArticle;
    private TextView sectionArticle;

    public ArticleViewHolder(View itemView) {
        super(itemView);
        imageViewArticle = (ImageView) itemView.findViewById(R.id.imageViewArticle);
        titleArticle = (TextView) itemView.findViewById(R.id.titleArticle);
        sectionArticle = (TextView) itemView.findViewById(R.id.sectionArticle);
    }

    public TextView getTitleArticle() {
        return titleArticle;
    }

    public TextView getSectionArticle() {
        return sectionArticle;
    }

    public ImageView getImageViewArticle() {
        return imageViewArticle;
    }

}


