package com.idix.maxime.idix_project.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Rweisha-PC on 25/04/2017.
 */

public class Article implements Parcelable {

    private int id;
    private String d;
    private String t;
    private Image i;
    private Section r;
    private String ch;
    private String co;
    private Author a;
    private ArrayList<ArticleRelated> al;

    public Article() {
    }

    public Article(int id, String d, String t, Image i, Section r, String ch, String co, Author a, ArrayList<ArticleRelated> al) {
        this.id = id;
        this.d = d;
        this.t = t;
        this.i = i;
        this.r = r;
        this.ch = ch;
        this.co = co;
        this.a = a;
        this.al = al;
    }

    protected Article(Parcel in) {
        id = in.readInt();
        d = in.readString();
        t = in.readString();
        ch = in.readString();
        co = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Image getI() {
        return i;
    }

    public void setI(Image i) {
        this.i = i;
    }

    public Section getR() {
        return r;
    }

    public void setR(Section r) {
        this.r = r;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public Author getA() {
        return a;
    }

    public void setA(Author a) {
        this.a = a;
    }

    public ArrayList<ArticleRelated> getAl() {
        return al;
    }

    public void setAl(ArrayList<ArticleRelated> al) {
        this.al = al;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", d='" + d + '\'' +
                ", t='" + t + '\'' +
                ", i=" + i +
                ", r=" + r +
                ", ch='" + ch + '\'' +
                ", co='" + co + '\'' +
                ", a=" + a +
                ", al=" + al +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(d);
        parcel.writeString(t);
        parcel.writeString(ch);
        parcel.writeString(co);
    }
}
